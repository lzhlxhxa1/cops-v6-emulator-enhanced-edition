﻿// *
// * ******** COPS v6 Emulator - Open Source ********
// * Copyright (C) 2015 CptSky
// *
// * Please read the WARNING, DISCLAIMER and PATENTS
// * sections in the LICENSE file.
// *

namespace COServer
{
    public static partial class Battle
    {
        /// <summary>
        /// The logger of the class.
        /// </summary>
        private static readonly log4net.ILog sLogger = log4net.LogManager.GetLogger(typeof(Battle));
    }
}
